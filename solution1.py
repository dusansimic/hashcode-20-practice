import sys
import numpy as np

f = open(sys.argv[1], 'r')

maxSlices = -1
numTypes = -1

firstLine = f.readline()
firstLine = firstLine.split(' ')
maxSlices = int(firstLine[0])
numTypes = int(firstLine[1])

numSlices = np.array([int(el) for el in f.readline().split(' ')])

resSlices = 0
resPizzas = []

for i in range(numTypes - 1, -1, -1):
	if resSlices + numSlices[i] <= maxSlices:
		resSlices += numSlices[i]
		resPizzas.append(i)


# print(len(resPizzas))
# print(' '.join([str(el) for el in resPizzas[::-1]]))
print(sum([numSlices[el] for el in resPizzas]))