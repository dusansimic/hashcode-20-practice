import sys
import numpy as np
from tqdm import tqdm

f = open(sys.argv[1], 'r')

maxSlices = -1
numTypes = -1

firstLine = f.readline()
firstLine = firstLine.split(' ')
maxSlices = int(firstLine[0])
numTypes = int(firstLine[1])

numSlices = [int(el) for el in f.readline().split(' ')]

# tmpSliceSum = 0
# sliceLowerBound = -1
# for i in range(0, numTypes):
# 	if tmpSliceSum >= maxSlices:
# 		sliceLowerBound = i - 1 if i != 0 else 0
# 		break
# 	tmpSliceSum += numSlices[i]

resPizzas = [[[], None] for i in range(numTypes)]
resSlices = [None for i in range(numTypes)]
for i in tqdm(range(numTypes - 1, -1, -1)):
	resSlices = 0
	for j in range(i, -1, -1):
		if resSlices + numSlices[j] <= maxSlices:
			resSlices += numSlices[j]
			resPizzas[i][0].append(j)
	resPizzas[i][1] = sum([numSlices[j] for j in resPizzas[i][0]])

maxResultIndex = np.argmax(resPizzas, axis=0)[1]
maxResult = resPizzas[maxResultIndex][0]
sumMaxResult = resPizzas[maxResultIndex][1]
# print(len(maxResult))
# print(' '.join([str(el) for el in maxResult[::-1]]))
print(sumMaxResult)
